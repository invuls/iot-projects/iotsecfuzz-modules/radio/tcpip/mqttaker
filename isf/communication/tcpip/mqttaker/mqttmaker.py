import paho.mqtt.client as mqtt
from isf.core import logger
import time


class MQTTMaker:
    client = None
    ip = '127.0.0.1'
    port = 1883
    connection = False
    loop = False

    message_array = []

    def __init__(self, domain,
                 port=1883,
                 login='nologin',
                 password='nopass',
                 timeout=60,
                 tls=False,
                 certfile='None',
                 keyfile='None',
                 version='MQTTv311',
                 transport="tcp",  # websockets
                 ws_path="/mqtt",
                 ws_headers="None"  # Auth::: 213456|||Cookie::: 12345
                 ):
        if version == 'MQTTv311':
            self.client = mqtt.Client(client_id="MqttClient",
                                      protocol=mqtt.MQTTv311,
                                      clean_session=True, transport=transport)
        else:
            self.client = mqtt.Client(client_id="MqttClient",
                                      protocol=mqtt.MQTTv31,
                                      clean_session=True, transport=transport)

        # Websockets options
        if transport == 'websockets':
            if ws_headers == "None":
                headers = None
            else:
                headers = {}
                try:
                    for header in ws_headers.split('|||'):
                        params = header.split(':::')
                        headers[params[0]] = params[1]
                except:
                    logger.error('Error while parsing websocket headers!')
            self.client.ws_set_options(path=ws_path, headers=headers)

        # tls options
        if tls:
            if certfile == 'None':
                certfile = None
            if keyfile == 'None':
                keyfile = None
            self.client.tls_set(ca_certs=None, certfile=certfile,
                                keyfile=keyfile, ciphers=None)
            self.client.tls_insecure_set(True)

        # auth options
        if login != 'nologin':
            if password == 'nopass':
                self.client.username_pw_set(login, password=None)
            else:
                self.client.username_pw_set(login, password=password)

        # connection
        try:
            self.client.connect(domain, port, keepalive=timeout)
            self.connection = True
        except:
            logger.error('Can\'t connect to MQTT server!')
            return

        # timeout package send
        # self.client.loop_start()

    def looping(self, val):
        if self.loop == val:
            return
        if val:
            self.client.loop_start()
        else:
            self.client.loop_stop()
        self.loop = val
        return

    def __del__(self):
        if self.connection:
            self.client.disconnect()
            self.looping(False)
            self.connection = False
        del self.message_array[:]

    def on_disconnect(self, client, userdata, rc=0):
        logger.debug("Disconnected result code " + str(rc))
        self.looping(False)

    def onmessage(self, client, userdata, msg):
        topic_printable = ''.join(
            [x for x in str(msg.topic) if x.isprintable()])[:20]
        message_printable = ''.join(
            [x for x in str(msg.payload.decode('charmap')) if x.isprintable()])[
                            :40]

        logger.info(
            'Topic: %s - Message: %s' % (topic_printable, message_printable))
        self.message_array.append([msg.topic, msg.payload.decode('charmap')])

    def reading_all(self, delay=10):
        if not self.connection:
            return
        start_time = time.time()
        # subscribe to all topics
        self.client.subscribe('#', qos=1)
        self.client.subscribe('$SYS/#')
        self.client.on_message = self.onmessage
        self.client.on_disconnect = self.on_disconnect
        self.looping(True)
        while time.time() - start_time < delay:
            pass
        self.looping(False)
        return self.message_array
